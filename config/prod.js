/* eslint-disable import/no-commonjs */
/* eslint-disable import/no-extraneous-dependencies */
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');

function buildConfig(env) {
  const config = Object.assign({}, require('./common')(env)); // eslint-disable-line

  config.output.filename = 'nav.js';

  Object.assign(config, {
    devtool: '#source-map',
    mode: 'production',
    optimization: {
      minimizer: [
        new TerserPlugin({
          cache: true,
          parallel: true,
          sourceMap: true, // set to true if you want JS source maps
        }),
        new OptimizeCSSAssetsPlugin({}),
      ],
    },
  });

  config.plugins.push(
    new HtmlWebpackPlugin({
      title: 'PRODUCTION prerender-spa-plugin',
      template: 'public/index.html',
      filename: `${env.dist}/index.html`,
    }),
    new MiniCssExtractPlugin({
      filename: '[name].css',
      chunkFilename: '[id].css',
    }),
  );

  return config;
}

module.exports = buildConfig;
